package db

import (
	"context"
	"errors"
	"log"

	"github.com/AlienList/backend-watchlist/bob"
	"github.com/google/uuid"

	"github.com/aarondl/opt/omit"
	"github.com/aarondl/opt/omitnull"
	"github.com/stephenafamo/bob/dialect/psql"
	"github.com/stephenafamo/bob/dialect/psql/sm"
)

func (db Database) GetAllWatchlists() (bob.WatchlistSlice, error) {
	ctx := context.Background()

	rows, err := bob.Watchlists.Query(ctx, db.Conn, sm.Columns(bob.Watchlists.Columns())).All()
	if err != nil {
		return rows, err
	}

	return rows, nil
}

func (db Database) AddWatchlist(watchlist *bob.Watchlist) error {
	ctx := context.Background()
	ret, err := bob.Watchlists.Insert(ctx, db.Conn, &bob.WatchlistSetter{
		AnimeID:           omit.From(watchlist.AnimeID),
		UserID:            omit.From(watchlist.UserID),
		Status:            omitnull.FromNull(watchlist.Status),
		Ratings:           omitnull.FromNull(watchlist.Ratings),
		AiringAt:          omitnull.FromNull(watchlist.AiringAt),
		NextAiringEpisode: omitnull.FromNull(watchlist.NextAiringEpisode),
	})
	if err != nil {
		return err
	}

	watchlist.ID = ret.ID

	return nil
}

func (db Database) GetWatchlistById(id uuid.UUID) (*bob.Watchlist, error) {
	ctx := context.Background()
	watchlist, err := bob.FindWatchlist(ctx, db.Conn, id)
	return watchlist, err
}

func (db Database) GetWatchlistByUserId(userId uuid.UUID) (bob.WatchlistSlice, error) {
	ctx := context.Background()
	watchlists, err := bob.Watchlists.Query(ctx, db.Conn, sm.Where(psql.Quote("user_id").EQ(psql.Arg(userId)))).All()
	if err != nil {
		return watchlists, err
	}
	return watchlists, err
}

func (db Database) GetAiringSchedulesByUserId(userId uuid.UUID) (bob.WatchlistSlice, error) {
	ctx := context.Background()
	watchlists, err := bob.Watchlists.Query(
		ctx, db.Conn, sm.Where(psql.And(
			psql.Quote("user_id").EQ(psql.Arg(userId)),
			psql.Quote("airing_at").IsNotNull(),
			psql.Quote("next_airing_episode").IsNotNull()))).All()

	if err != nil {
		return watchlists, err
	}
	return watchlists, err
}

func (db Database) DeleteWatchlist(id uuid.UUID) error {
	ctx := context.Background()
	watchlist, err := bob.FindWatchlist(ctx, db.Conn, id)
	if err != nil {
		return errors.New("watchlist does not exist")
	}
	err = watchlist.Delete(ctx, db.Conn)
	if err != nil {
		return errors.New("unable to delete record")
	}
	return err
}

func (db Database) UpdateWatchlist(id uuid.UUID, watchlistData *bob.Watchlist) (*bob.Watchlist, error) {
	ctx := context.Background()
	tx, err := db.Conn.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := tx.Rollback(); err != nil {
			log.Println(err)
		}
	}()

	user, err := bob.FindWatchlist(ctx, tx, id)
	if err != nil {
		return watchlistData, err
	}
	var updateWatchlist = &bob.WatchlistSetter{}
	updateWatchlist.Ratings = omitnull.FromNull(watchlistData.Ratings)
	updateWatchlist.Status = omitnull.FromNull(watchlistData.Status)
	updateWatchlist.AiringAt = omitnull.FromNull(watchlistData.AiringAt)
	updateWatchlist.NextAiringEpisode = omitnull.FromNull(watchlistData.NextAiringEpisode)

	if watchlistData.AnimeID >= 0 {
		updateWatchlist.AnimeID = omit.From(watchlistData.AnimeID)
	}

	if len(watchlistData.UserID) > 0 {
		updateWatchlist.UserID = omit.From(watchlistData.UserID)
	}

	err = user.Update(ctx, tx, updateWatchlist)
	if err != nil {
		return watchlistData, err
	}

	watchlistData.ID = id
	if err := tx.Commit(); err != nil {
		return nil, err
	}

	return watchlistData, nil

}
