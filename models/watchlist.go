package models

import (
	"fmt"
	"net/http"

	"github.com/AlienList/backend-watchlist/bob"
	"github.com/google/uuid"
)

type WatchRequest struct {
	*bob.Watchlist
	Genres []string `json:"genres"`
}

func (watchlist *WatchRequest) Bind(r *http.Request) error {
	if watchlist.UserID == uuid.Nil {
		return fmt.Errorf("user id is a required field")
	}
	if watchlist.AnimeID == 0 {
		return fmt.Errorf("anime id is a required field")
	}

	return nil
}

type WatchlistUpdateRequest struct {
	*bob.Watchlist
}

func (watchlist *WatchlistUpdateRequest) Bind(r *http.Request) error {
	if watchlist.Watchlist == nil {
		return fmt.Errorf("Watchlist fields not matched")
	}

	return nil
}
